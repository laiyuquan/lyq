<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'DXY\\BIZ\\SIM\\' => array($vendorDir . '/dxy/biz-sim'),
    'App\\' => array($baseDir . '/app'),
);
